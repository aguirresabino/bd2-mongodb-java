/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifpb.dao;

import br.com.ifpb.model.Publicacao;
import br.com.ifpb.factory.ConFactory;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aguirre
 */
public class PublicacaoDaoMongo {
    public static boolean create(Publicacao p){
        MongoDatabase db = ConFactory.getConnection();
        
        MongoCollection collection = db.getCollection("Publicacao", Publicacao.class);
        
        collection.insertOne(p);
        return collection.find(and(eq("autor", p.getAutor()), eq("titulo", p.getTitulo()))) != null;
    }
    
    public static void createMany(Publicacao... publicacoes){
        MongoDatabase db = ConFactory.getConnection();
        MongoCollection collection = db.getCollection("Publicacao", Publicacao.class);
        
        List<Publicacao> pubList = new ArrayList<>();
        
        for(Publicacao p : publicacoes){
            pubList.add(p);
        }
        
        collection.insertMany(pubList);
    }
    
    public static List<Publicacao> read(){
        List<Publicacao> publicacoes = new ArrayList<>();
        MongoDatabase db = ConFactory.getConnection();
        
        MongoCollection collection = db.getCollection("Publicacao", Publicacao.class);
        
        
        MongoCursor cursor = collection.find().iterator();
        
        while(cursor.hasNext()){
            publicacoes.add((Publicacao) (cursor.next()));
        }
        
        return publicacoes;
        
    }
    
    public static List<Publicacao> readByContent(String content){
        List<Publicacao> publicacoes = new ArrayList<>();
        MongoDatabase db = ConFactory.getConnection();
        
        MongoCollection collection = db.getCollection("Publicacao", Publicacao.class);
        
        MongoCursor cursor = collection.find(Filters.text(content)).iterator();
        
        while(cursor.hasNext()){
            publicacoes.add((Publicacao) (cursor.next()));
        }
        
        return publicacoes;
    }
}
